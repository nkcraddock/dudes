﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nonsense;

namespace UnitTestProject1
{
    [TestClass]
    public class CalculatorTest
    {
        FakeCommandSource cmd;
        Calculator calculator;
        
        [TestInitialize]
        public void TestSetup()
        {
            cmd = new FakeCommandSource();
            calculator = new Calculator(cmd);
        }

        [TestMethod]
        public void ShouldStoreNumbersAsArguments()
        {
            cmd.Send(2, 3);
            Assert.AreEqual<int>(3, calculator.Result());
        }

        [TestMethod]
        public void ShouldDoMath()
        {
            // (2 + 3) * (5 - 3) % 10 == 0
            cmd.Send(2, 3, "+", 5, 3, "-", "*", 10, "%");
            Assert.AreEqual<int>(0, calculator.Result());

            // (2 * 10) / (2 + 2) - (6 % 4) + 3 == 6 
            cmd.Send(2, 10, "*", 2, 2, "+", "/", 6, 4, "%", "-", 3, "+");
            Assert.AreEqual<int>(6, calculator.Result());
        }
    }

    public class FakeCommandSource : CommandSource
    {
        public event EventHandler<string> CommandReceived;

        public void Send(params object[] commands)
        {
            foreach (var command in commands)
            {
                CommandReceived(this, command.ToString());
            }
        }
    }

}
