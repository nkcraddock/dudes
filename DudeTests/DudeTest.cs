﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nonsense;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace UnitTestProject1
{
    [TestClass]
    public class DudeTest
    {
        DudeRepo dudes;

        [TestInitialize]
        public void TestSetup()
        {
            dudes = new DudeRepo();
            dudes.AddDude(new Dude("Lebowski"));
        }

        [TestMethod]
        public void DudesArePersisted()
        {
            var dude = dudes.Get("Lebowski");
            Assert.IsNotNull(dude);
            Assert.AreEqual<string>("Lebowski", dude.Name);
        }

        [TestMethod]
        public void DudeSavePersitsChanges()
        {
            var dude = dudes.Get("Lebowski");
            Assert.IsNotNull(dude);
            dude.Name = "Jeffrey";
            dude.Save();

            var allDudes = dudes.GetDudes();
            Assert.AreEqual<int>(1, allDudes.Count);
            Assert.AreEqual<string>("Jeffrey", allDudes[0].Name);
        }

        [TestMethod]
        public void RecognizesGoodDudes()
        {
            var dude = dudes.Get("Lebowski");
            Assert.IsNotNull(dude);
            Assert.IsFalse(dude.Good);

            dude.GivePoints(100);
            Assert.IsTrue(dude.Good);
        }

        [TestMethod]
        // This one might take more time than we have
        public void WereGonnaNeedABiggerBoat()
        {
            var alotOfDudes = new List<Dude>();
            Parallel.For(0, 1000, i =>
            {
                var newdude = new Dude(string.Format("Dude #{0}", i));
                if (i == 980)
                    newdude.GivePoints(100);

                dudes.AddDude(newdude);
            });

            var dude = dudes.Get("Dude #980");
            Assert.IsNotNull(dude);
            Assert.IsTrue(dude.Good);

            // Make sure you didnt cheat and make them all good
            dude = dudes.Get("Dude #1000");
            Assert.IsNotNull(dude);
            Assert.IsFalse(dude.Good);
        }
    }
}
