﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nonsense
{
    /// A source of commands which are a string containing one of:
    ///     Number (something you can int.parse)
    ///     Operation ( '+', '-', '/', '*', '%' )
    public interface CommandSource
    {
        event EventHandler<string> CommandReceived;
    }

    /// Accepts a command source and performs the commands recieved by
    /// the "CommandReceived" by events, storing the latest result and returning it
    /// when Calculator.Result() is called
    /// 
    /// This is a reverse polish notation calculator and for this test, order of operations is not important
    ///     so when a number is received it is added to the list of arguments
    ///     When an operation is received, it is performed on the most recent two arguments and the result is added to the list of arguments
    ///     When Result() is called, the latest current argument is returned as the value
    public class Calculator
    {

        public Calculator(CommandSource source)
        {

        }

        public int Result()
        {
            return 0;
        }
    }
}
