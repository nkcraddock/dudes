﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nonsense
{
    public class Dude
    {
        public string Name { get; set; }
        public bool Good { get; private set; }
        public int Points { get; private set; }

        public Dude(string name)
        {
            this.Name = name;
        }

        public void GivePoints(int points)
        {

        }

        public void Save()
        {

        }
    }

    public class DudeRepo
    {
        // Returns a list of dudes. If "good" is passed, should filter to only dudes
        // matching the good criteria 
        public List<Dude> GetDudes(bool? good = null)
        {
            return null;
        }

        // Returns a dude with the matching name
        public Dude Get(string name)
        {
            return null;
        }

        public void AddDude(Dude dude)
        {
        }
    }
}
